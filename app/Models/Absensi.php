<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Absensi extends Model
{
    protected $table = "absensi";
    protected $fillable = [
        "tanggal",
        "status",
        "keterangan",
        "siswa_id",
        "petugas_id",
        "tahun_ajaran_id",
    ];

    function siswa(){
        return $this->belongsTo("App\Models\Siswa");
    }
}
