<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KategoriPenghargaan extends Model
{
    protected $table = "kategori_penghargaan";
    protected $fillable = [
        "pasal"
    ];
}
