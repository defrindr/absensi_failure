<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KategoriPeraturan extends Model
{
    protected $table = "kategori_peraturan";
    protected $fillable = [
        "pasal",
    ];

    public function peraturan() {
        return $this->hasMany(Peraturan::class);
    }
}
