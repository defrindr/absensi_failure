<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    protected $table = "kelas";
    protected $fillable = [
        "kelas",
        "grade",
        "jurusan_id",
        "nama_kelas",
        "wali_kelas_id",
        "tahun_ajaran_id",
    ];

    function getTotalPoint($column,$id){
        $pointKelas = $this->join('siswa','kelas_id','kelas.id')
            ->groupBy('kelas_id')
            ->having('kelas_id',$id)
            ->sum( $column );
        return $pointKelas. " Point";
    }

    function siswa() {
        return $this->hasMany(Siswa::class);
    }
    function jurusan() {
        return $this->belongsTo(Jurusan::class);
    }
    function petugas() {
        return $this->belongsTo(User::class,'petugas_id');
    }
}
