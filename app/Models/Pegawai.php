<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    protected $table = 'pegawai';
    protected $fillable = [
        'nama',
        'alamat',
        'agama',
        'jenis_kelamin',
        'status_kepegawaian',
        'asal_instansi',
        'jabatan',
        'no_telp',
        'foto',
    ];

    function walikelas(){
        return $this->hasOne(WaliKelas::class);
    }

    function user(){
        return $this->hasOne(User::class);
    }
}
