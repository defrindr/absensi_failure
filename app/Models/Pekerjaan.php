<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pekerjaan extends Model
{
    protected $table = 'pekerjaan_wali_murid';
    protected $fillable = [
        'nama_pekerjaan'
    ];

    protected function waliMurid(){
        return $this->hasMany(WaliMurid::class);
    }
}
