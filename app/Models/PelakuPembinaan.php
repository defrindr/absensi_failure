<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PelakuPembinaan extends Model
{
    protected $table = 'pelaku_pembinaan';
    protected $fillable = [
        "pembina"
    ];

    function pelakuPembinaanonPembinaan(){
        return $this->hasMany(PelakuPembinaanonPembinaan::class);
    }
    
}
