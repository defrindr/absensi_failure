<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PelakuPembinaanOnPembinaan extends Model
{
    protected $table = 'pelaku_pembinaan_on_pembinaan';
    protected $fillable = [
        "pembinaan_id",
        "pelaku_pembinaan_id"
    ];

    function pembinaan(){
        return $this->belongsTo(Pembinaan::class);
    }
    function pelakuPembinaan(){
        return $this->belongsTo(PelakuPembinaan::class);
    }
}
