<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pelanggaran extends Model
{
    protected $table = "pelanggaran";
    protected $fillable = [
        "tanggal",
        "siswa_id",
        "point_id",
        "user_id",
    ];

    function point() {
        return $this->belongsTo('App\Models\Point');
    }
    function siswa() {
        return $this->belongsTo('App\Models\Siswa');
    }
}
