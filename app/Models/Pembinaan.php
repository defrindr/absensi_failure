<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pembinaan extends Model
{
    protected $table = 'pembinaan';
    protected $fillable = [
        'klasifikasi',
        'point_minimal',
        'point_maksimal',
        'punishment_id'
    ];

    function siswaOnPembinaan(){
        return $this->hasMany(SiswaOnPembinaan::class);
    }
    function punishment(){
        return $this->belongsto(Punishment::class);
    }

    function pelakuPembinaanonPembinaan(){
        return $this->hasMany(PelakuPembinaanonPembinaan::class);
    }

    function getPembina(){
        return $this->pelakuPembinaanonPembinaan
            ->where(['id_pembinaan' => $this->id])
            ->get();
    }
}
