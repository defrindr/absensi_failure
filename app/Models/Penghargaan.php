<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Penghargaan extends Model
{
    protected $table = "penghargaan";
    protected $fillable = [
        "perihal",
        "point",
        "kategori_penghargaan"
    ];

    function kategoriPenghargaan(){
        return $this->belongsTo(KategoriPenghargaan::class);
    }
}
