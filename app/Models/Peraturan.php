<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Peraturan extends Model
{
    protected $table = "peraturan";
    protected $fillable = [
        "perihal",
        "point",
        "kategori_peraturan_id",
        "tindakan_id",
    ];

    public function getIntervalWaktu(){
        return $this->interval_waktu. " Kali";
    }

    public function getperaturan(){
        return $this->peraturan. " peraturan";
    }


    public function kategori_peraturan(){
        return $this->belongsTo(Kategoriperaturan::class);
    }

    function kasus() {
        return $this->hasMany('App\Models\Kasus');
    }

    function kategoriperaturan() {
        return $this->belongsTo('App\Models\Kategoriperaturan');
    }
}
