<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Punishment extends Model
{
    protected $table = 'punishment';
    protected $fillable = [
        'punishment'
    ];

    function pembinaan(){
        return $this->hasMany(Pembinaan::class);
    }
}
