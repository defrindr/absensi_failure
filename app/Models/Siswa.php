<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $table = "siswa";
    protected $fillable = [
        "nis",
        "kelas_id",
        "nama",
        "agama",
        "alamat",
        "domisili",
        "tempat_lahir",
        "tanggal_lahir",
        "no_telp",
        "foto",
        "point_pelanggaran",
        "point_penghargaan",
    ];

    function pelanggaran() {
        return $this->hasMany(Pelanggaran::class);
    }
    function absensi() {
        return $this->hasMany(Absensi::class);
    }
    function kelas() {
        return $this->belongsTo(Kelas::class);
    }
}
