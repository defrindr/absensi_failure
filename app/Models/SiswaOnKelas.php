<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SiswaOnKelas extends Model
{
    protected $table = 'siswa_on_kelas';
    protected $fillable = [
        'siswa_id',
        'kelas_id'
    ];

    function kelas(){
        return $this->belongsTo(Kelas::class);
    }
    function siswa(){
        return $this->belongsTo(Siswa::class);
    }
}
