<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SiswaOnPembinaan extends Model
{
    protected $table = 'siswa_on_pembinaan';
    protected $fillable = [
        "tanggal",
        "pembinaan_id",
        "jumlah_point",
        "siswa_id"
    ];

    function siswa(){
        return $this->belongsTo(Siswa::class);
    }
    function pembinaan(){
        return $this->belongsTo(Pembinaan::class);
    }
}
