<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SiswaOnPenghargaan extends Model
{
    protected $table = 'siswa_on_penghargaan';
    protected $fillable = [
        'penghargaan_id',
        'siswa_id',
        'tahun_ajaran_id',
    ];

    function siswa(){
        return $this->belongsTo(Siswa::class);
    }
    function penghargaan(){
        return $this->belongsTo(Penghargaan::class);
    }
}
