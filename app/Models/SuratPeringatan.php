<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SuratPeringatan extends Model
{
    protected $table = 'surat_peringaran';
    protected $fillable = [
        'sp_ke',
        'siswa_id',
        'tanggal',
        'jumlah_point'
    ];

    function siswa(){
        return $this->belongsTo(Siswa::class);
    }
}
