<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = "users";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'pegawai_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    function absensi(){
        return $this->hasMany(Absensi::class);
    }

    function pelanggaran(){
        return $this->hasMany(Pelanggaran::class);
    }

    function role(){
        return $this->hasMany(UserRoles::class);
    }

    
    function getRoleName(){
        return $this->userRole($user->id)
            ->select(['roles.nama'])
            ->get()->toArray();
    }
}
