<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WaliKelas extends Model
{
    protected $table = 'wali_kelas';
    protected $fillable = [
        'pegawai_id',
        'kelas_id',
    ];

    function kelas(){
        return $this->belongsTo(Kelas::class);
    }
    function pegawai(){
        return $this->belongsTo(Pegawai::class);
    }
}
