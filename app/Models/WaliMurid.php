<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WaliMurid extends Model
{
    protected $table = 'wali_murid';
    protected $fillable = [
        'nik',
        'nama',
        'jenis_kelamin',
        'alamat',
        'no_telp',
        'pekerjaan_id',
    ];

    function anak(){
        return $this->hasMany(Siswa::class);
    }
}
