<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Pegawai;
use Faker\Generator as Faker;

$factory->define(Pegawai::class, function (Faker $faker) {
    return [
        'nama' => $faker->name,
        'alamat' => $faker->address,
        'agama' => 'Islam',
        'jenis_kelamin'=> 'laki-laki',
        'status_kepegawain' => 'Pegawai Tetap',
        'asal_instansi' => 'SMKN 1 Jenangan',
        'jabatan' => 'Guru',
        'no_telp' => $faker->phoneNumber,
        'foto' => 'lorem_ipsum.png'
    ];
});
