<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Siswa;
use Faker\Generator as Faker;

$factory->define(Siswa::class, function (Faker $faker) {
    return [
        "nis" => $faker->numberBetween(111111111,99999999),
        "nama" => $faker->name,
        "alamat" => $faker->address,
        "agama" => "Islam",
        "domisili" => $faker->address,
        "tempat_lahir" => $faker->address,
        "tanggal_lahir" => $faker->date,
        "no_telp" => $faker->phoneNumber,
        "point_pelanggaran" => "0",
        "point_penghargaan"=> "0",
        'foto' => 'lorem_ipsum.png',
        'wali_murid_id' => $faker->unique()->numberBetween(1,100),
    ];
});
