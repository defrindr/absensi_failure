<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\SiswaOnKelas;
use Faker\Generator as Faker;

$factory->define(SiswaOnKelas::class, function (Faker $faker) {
    return [
        "siswa_id" => $faker->numberBetween(1,80),
        "kelas_id" => $faker->numberBetween(1,2)
    ];
});
