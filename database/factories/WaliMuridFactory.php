<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\WaliMurid;
use Faker\Generator as Faker;

$factory->define(WaliMurid::class, function (Faker $faker) {
    return [
        'nik' => $faker->numberBetween(11111111111111111,99999999999999999),
        'nama' => $faker->name,
        'jenis_kelamin' => 'laki-laki',
        'alamat' => $faker->address,
        'no_telp' => $faker->phoneNumber,
        'pekerjaan_id' => $faker->numberBetween(1,4),
    ];
});
