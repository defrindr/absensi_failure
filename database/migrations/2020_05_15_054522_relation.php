<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Relation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        // wali

        Schema::table('wali_kelas',function(Blueprint $table){
            $table->foreignId('pegawai_id')->constrained('pegawai');
        });

        Schema::table('wali_murid',function(Blueprint $table){
            $table->foreignId('pekerjaan_id')->constrained('pekerjaan_wali_murid');
        });

        // user
         
        Schema::table('users', function(Blueprint $table){
            $table->foreignId('pegawai_id')->constrained('pegawai');
        });

        Schema::table('user_roles', function (Blueprint $table) {
            $table->foreignId("role_id")->constrained();
            $table->foreignId("user_id")->constrained();
        });

        // kelas
        Schema::table('kelas', function (Blueprint $table) {
            $table->foreignId("jurusan_id")->constrained("jurusan");
            $table->foreignId("wali_kelas_id")->constrained("wali_kelas");
            $table->foreignId('tahun_ajaran_id')->constrained('tahun_ajaran');
        });

        Schema::table('siswa_on_kelas', function (Blueprint $table) {
            $table->foreignId("siswa_id")->constrained("siswa");
            $table->foreignId("kelas_id")->constrained("kelas");
        });

        Schema::table('siswa', function(Blueprint $table){
            $table->foreignId('wali_murid_id')->constrained('wali_murid');
        });


        // pelanggaran

        Schema::table('peraturan', function (Blueprint $table) {
            $table->foreignId("kategori_peraturan_id")->constrained("kategori_peraturan");
            $table->foreignId('tindakan_id')->constrained('tindakan');
        });

        Schema::table('pelanggaran', function (Blueprint $table) {
            $table->foreignId("siswa_id")->constrained("siswa")->onDelete('cascade');
            $table->foreignId("peraturan_id")->constrained("peraturan");
            $table->foreignId("petugas_id")->constrained("users");
            $table->foreignId('tahun_ajaran_id')->constrained('tahun_ajaran');
        });

        Schema::table('absensi', function (Blueprint $table) {
            $table->foreignId("siswa_id")->constrained("siswa");
            $table->foreignId("tahun_ajaran_id")->constrained("tahun_ajaran");
            $table->foreignId("petugas_id")->constrained("users");
        });

        Schema::table('surat_peringatan', function(Blueprint $table) {
            $table->foreignId('siswa_id')->constrained('siswa');
        });

        Schema::table('pembinaan', function(blueprint $table){
            $table->foreignId('punishment_id')->constrained('punishment');
        });

        Schema::table('siswa_on_pembinaan',function(Blueprint $table){
            $table->foreignId('siswa_id')->constrained('siswa');
            $table->foreignId('pembinaan_id')->constrained('pembinaan');
        });

        Schema::table('pelaku_pembinaan_on_pembinaan', function(Blueprint $table){
            $table->foreignId('pelaku_pembinaan_id')->constrained('pelaku_pembinaan');
            $table->foreignId('pembinaan_id')->constrained('pembinaan');
        });

        // penghargaan

        Schema::table('penghargaan', function(Blueprint $table){
            $table->foreignId('kategori_penghargaan_id')->constrained('kategori_penghargaan');
        });

        Schema::table('siswa_on_penghargaan',function(Blueprint $table){
            $table->foreignId('siswa_id')->constrained('siswa');
            $table->foreignId('penghargaan_id')->constrained('penghargaan');
            $table->foreignId('tahun_ajaran_id')->constrained('tahun_ajaran');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siswa', function (Blueprint $table) {
            //
        });
    }
}
