<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // call seeder
        $this->call(PegawaiSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(UserRolesSeeder::class);
        
        $this->call(TahunAjaranSeeder::class);
        $this->call(WaliKelasSeeder::class);
        $this->call(HariEfektifSeeder::class);
        $this->call(HariTidakEfektifSeeder::class);

        $this->call(JurusanSeeder::class);
        $this->call(KelasSeeder::class);

        $this->call(KategoriPenghargaanSeeder::class);
        $this->call(PenghargaanSeeder::class);
        $this->call(KategoriPeraturanSeeder::class);
        $this->call(TindakanSeeder::class);
        $this->call(PeraturanSeeder::class);
        $this->call(PunishmentSeeder::class);

        $this->call(PekerjaanSeeder::class);
        $this->call(WaliMuridSeeder::class);
        $this->call(SiswaSeeder::class);
    }
}
