<?php

use Illuminate\Database\Seeder;
use App\Models\Jurusan;


class JurusanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jurusan = [
            [
                "nama" => "Rekayasa Perangkat Lunak",
                "kepala_jurusan" => "Drs. Soekarno",
            ],
            [
                "nama" => "Elektronika Industri",
                "kepala_jurusan" => "Drs. Soekarno",
            ],
            [
                "nama" => "Otomasi Industri",
                "kepala_jurusan" => "Drs. Soekarno",
            ],
            [
                "nama" => "Teknik Pemesinan",
                "kepala_jurusan" => "Drs. Soekarno",
            ],
            [
                "nama" => "Teknik dan Bisnis Sepeda Motor",
                "kepala_jurusan" => "Drs. Soekarno",
            ],
            [
                "nama" => "Teknik Pendingin",
                "kepala_jurusan" => "Drs. Soekarno",
            ],
            [
                "nama" => "Bisnis Konstruksi dan Properti",
                "kepala_jurusan" => "Drs. Soekarno",
            ],
            [
                "nama" => "Desain Permodelan dan Informasi Bangunan",
                "kepala_jurusan" => "Drs. Soekarno",
            ],
            [
                "nama" => "Teknik Pengelasan",
                "kepala_jurusan" => "Drs. Soekarno",
            ],
        ];

        foreach($jurusan as $row){
            jurusan::create($row);
        }
    }
}
