<?php

use Illuminate\Database\Seeder;
use App\Models\KategoriPenghargaan;


class KategoriPenghargaanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $KategoriPenghargaan = [
            [
                "pasal" => "Membawa Nama Baik Sekolah"
            ]
        ];

        foreach($KategoriPenghargaan as $row){
            KategoriPenghargaan::create($row);
        }

    }
}
