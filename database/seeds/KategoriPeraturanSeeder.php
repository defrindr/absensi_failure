<?php

use Illuminate\Database\Seeder;
use App\Models\KategoriPeraturan;


class KategoriPeraturanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kategori = [
            [
                "pasal" => "KEHADIRAN",
            ],
            [
                "pasal" => "KEGIATAN PEMBELAJARAN",
            ],
            [
                "pasal" => "PAKAIAN SERAGAM",
            ],
            [
                "pasal" => "MAKAN DAN MINUM",
            ],
            [
                "pasal" => "TINDAKAN KENAKALAN DAN KRIMINALITAS",
            ],
            [
                "pasal" => "PRAKERIN",
            ],
        ];

        foreach($kategori as $row){
            KategoriPeraturan::create($row);
        }

    }
}
