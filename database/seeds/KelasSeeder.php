<?php

use Illuminate\Database\Seeder;
use App\Models\Kelas;

class KelasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kelas = [
            [
                "kelas" => "XII",
                "grade" => "A",
                "nama_kelas" => "XII Rekayasa Perangkat Lunak A",
                "jurusan_id" => 1,
                "tahun_ajaran_id" => 2,
                "wali_kelas_id" => 2,
            ],
            [
                "kelas" => "XII",
                "grade" => "B",
                "nama_kelas" => "XII Rekayasa Perangkat Lunak B",
                "jurusan_id" => 1,
                "tahun_ajaran_id" => 2,
                "wali_kelas_id" => 3,
            ],
        ];

        foreach($kelas as $row) {
            Kelas::create($row);
        }
    }
}
