<?php

use Illuminate\Database\Seeder;
use App\Models\Pegawai;
class PegawaiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Pegawai::class, 20)->create();;
    }
}
