<?php

use Illuminate\Database\Seeder;
use App\Models\Pekerjaan;

class PekerjaanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pekerjaan = [
            [
                'nama_pekerjaan' => 'PNS'
            ],
            [
                'nama_pekerjaan' => 'Petani'
            ],
            [
                'nama_pekerjaan' => 'Wiraswasta'
            ],
            [
                'nama_pekerjaan' => 'Tidak Bekerja'
            ],
        ];

        foreach($pekerjaan as $row){
            Pekerjaan::create($row);
        }
    }
}
