<?php

use Illuminate\Database\Seeder;
use App\Models\PelakuPembinaan;

class PelakuPembinaanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $PelakuPembinaan = [
            [
                "pembina" => "Kepala Sekolah",
            ],
            [
                "pembina" => "Wali Kelas",
            ],
            [
                "pembina" => "Guru Konseling",
            ],
            [
                "pembina" => "Kabid Keahlian",
            ],
        ];

        foreach($PelakuPembinaan as $row){
            PelakuPembinaan::create($row);
        }

    }
}
