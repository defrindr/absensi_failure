<?php

use Illuminate\Database\Seeder;
use App\Models\Penghargaan;

class PenghargaanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $penghargaan = [
            [
                "perihal" => "Membawa nama baik sekolah ke tingkat Nasional",
                "point" => 50,
                "kategori_penghargaan_id" => 1,
            ],
            [
                "perihal" => "Membawa nama baik sekolah ke tingkat Provinsi",
                "point" => 40,
                "kategori_penghargaan_id" => 1,
            ],
            [
                "perihal" => "Membawa nama baik sekolah ke tingkat Kota/Kabupaten",
                "point" => 25,
                "kategori_penghargaan_id" => 1,
            ],
            [
                "perihal" => "Membawa nama baik sekolah ke tingkat Kecamatan",
                "point" => 15,
                "kategori_penghargaan_id" => 1,
            ],
        ];

        foreach($penghargaan as $row){
            Penghargaan::create($row);
        }

    }
}
