<?php

use Illuminate\Database\Seeder;
use App\Models\Peraturan;

class PeraturanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $point = [
            [
                "perihal" => "Datang terlambat 1-15 menit",
                "point" => 3,
                "kategori_peraturan_id" => 2,
                "tindakan_id" => 2,
            ],
            [
                "perihal" => "Datang terlambat 16-30 menit",
                "point" => 4,
                "kategori_peraturan_id" => 2,
                "tindakan_id" => 2,
            ],
            [
                "perihal" => "Tidak masuk sekolah tanpa izin per hari",
                "point" => 10,
                "kategori_peraturan_id" => 2,
                "tindakan_id" => 1,
            ],
            [
                "perihal" => "Tidak masuk sekolah tanpa izin (>=3 hari) per hari",
                "point" => 10,
                "kategori_peraturan_id" => 2,
                "tindakan_id" => 1,
            ],
        ];
        
        foreach($point as $row){
            Peraturan::create($row);
        }

    }
}
