<?php

use Illuminate\Database\Seeder;
use App\Models\Punishment;
class PunishmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Punishment = [
            [
                "punishment" => "Membuat surat pernyataan",
            ],
            [
                "punishment" => "Skorsing 3 hari",
            ],
            [
                "punishment" => "Skorsing 6 hari",
            ],
            [
                "punishment" => "Dikeluarkan",
            ],
        ];

        foreach($Punishment as $row){
            Punishment::create($row);
        }
    }
}
