<?php

use Illuminate\Database\Seeder;
use App\Models\SiswaOnKelas;
class SiswaOnKelasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(SiswaOnKelas::class, 32);
    }
}
