<?php

use Illuminate\Database\Seeder;
use App\Models\SiswaOnPenghargaan;

class SiswaOnPenghargaanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $SiswaOnPenghargaan = [
            [
                "siswa_id" => 1,
                "penghargaan_id" => 1,
            ],
            [
                "siswa_id" => 1,
                "penghargaan_id" => 2,
            ],
            [
                "siswa_id" => 2,
                "penghargaan_id" => 2,
            ]
        ];

        foreach($SiswaOnPenghargaan as $row){
            SiswaOnPenghargaan::create($row);
        }

    }
}
