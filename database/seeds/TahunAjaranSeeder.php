<?php

use Illuminate\Database\Seeder;
use App\Models\TahunAjaran;
class TahunAjaranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tahunAjaran = [
            [
                "tahun_ajaran" => "2018/2019",
            ],
            [
                "tahun_ajaran" => "2019/2020",
            ],
            [
                "tahun_ajaran" => "2020/2021",
            ],
        ];
        
        foreach($tahunAjaran as $row){
            TahunAjaran::create($row);
        }
    }
}
