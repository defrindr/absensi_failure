<?php

use Illuminate\Database\Seeder;
use App\Models\Tindakan;
class TindakanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tindakan = [
            [
                "tindakan" => "Peringatan Lisan"
            ],
            [
                "tindakan" => "Membersihkan Lingkungan Sekolah"
            ],
        ];

        foreach($tindakan as $row){
            Tindakan::create($row);
        }
    }
}
