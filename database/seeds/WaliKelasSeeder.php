<?php

use Illuminate\Database\Seeder;
use App\Models\WaliKelas;

class WaliKelasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $walikelas = [
            [
                "pegawai_id" => 4,
            ],
            [
                "pegawai_id" => 5,
            ],
            [
                "pegawai_id" => 6,
            ],
            [
                "pegawai_id" => 7,
            ],
        ];

        foreach($walikelas as $row){
            WaliKelas::create($row);
        }

    }
}
