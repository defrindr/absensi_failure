<?php

use Illuminate\Database\Seeder;
use App\Models\WaliMurid;

class WaliMuridSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(WaliMurid::class, 100)->create();
    }
}
